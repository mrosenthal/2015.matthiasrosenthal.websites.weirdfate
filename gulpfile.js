var gulp = require('gulp');
var webserver = require('gulp-webserver');
var gutil = require('gulp-util');
var bower = require('bower');
var concat = require('gulp-concat');
var sass = require('gulp-sass');
var minifyCss = require('gulp-minify-css');
var rename = require('gulp-rename');
var sh = require('shelljs');
var stripDebug = require('gulp-strip-debug');
var uglify = require('gulp-uglify');
var ngmin = require('gulp-ngmin');
var jshint = require('gulp-jshint');
var cache = require('gulp-cache');

var paths = {
	sass : ['css/scss/**/*.scss']
};

gulp.task('sass', function(done) {
	gulp.src('css/scss/main.scss').pipe(sass()).pipe(gulp.dest('css/')).pipe(minifyCss({
		keepSpecialComments : 0
	})).pipe(rename({
		extname : '.min.css'
	})).pipe(gulp.dest('css/')).on('end', done);
});

gulp.task('watch', function() {
	gulp.watch(paths.sass, ['sass']);
});

gulp.task('install', ['git-check'], function() {
	return bower.commands.install().on('log', function(data) {
		gutil.log('bower', gutil.colors.cyan(data.id), data.message);
	});
});

gulp.task('git-check', function(done) {
	if (!sh.which('git')) {
		console.log('  ' + gutil.colors.red('Git is not installed.'), '\n  Git, the version control system, is required to download Ionic.', '\n  Download git here:', gutil.colors.cyan('http://git-scm.com/downloads') + '.', '\n  Once git is installed, run \'' + gutil.colors.cyan('gulp install') + '\' again.');
		process.exit(1);
	}
	done();
});

gulp.task('serve', function() {
	gulp.src('').pipe(webserver({
		fallback : 'index.html',
		livereload : true,
		directoryListing : true,
		host : "localhost",
		port : 5050,
		open : 'http://localhost:5050/index.html'
	}));
	
	gulp.watch(paths.sass, ['sass']);
});

gulp.task('minify-scripts', function() {
	gulp.src(['js/**/*.js']).pipe(concat('main.min.js')).pipe(stripDebug()).pipe(ngmin({
		dynamic : true
	})).pipe(uglify({
		mangle : false
	})).pipe(gulp.dest('js'));
});

gulp.task('clearCache', function() {
  // Still pass the files to clear cache for
  gulp.src('*.*')
    .pipe(cache.clear());

  // Or, just call this for everything
  cache.clearAll();
});

// JS hint task
gulp.task('jshint', function() {
	gulp.src(['js/**/*.js']).pipe(jshint()).pipe(jshint.reporter('default'));
});