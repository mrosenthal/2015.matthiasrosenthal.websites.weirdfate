var application = angular.module('starter', ['ngSanitize', 'jsonService', 'ngProgress', 'ngAnimate']);

application.run(function(ngProgress) {
	//start progress bar
	ngProgress.color("#8A8173");
	ngProgress.height("7px");
	ngProgress.start();

	/* Scroll configurartion */
	AA_CONFIG = {
		animationLength : 1200,
		easingFunction : 'easeOutExpo',
		scrollOffset : 65
	};

	/* Set default location hash */
	location.hash = "##news";

	/* resize handler */
	$(window).resize(function() {
		$(".background").height(($(".content").height()) / 3);
		$(".background-container").height($(".content").height());
	});
});
