/**
 * @ngdoc overview
 *
 * @name js.controller:appController
 * @module js.controller
 * @description
 * Main app controller overall for all views and components.
 *
 * Init google service if needed.
 *
 */

application.controller('AppCtrl', function($scope, $rootScope, $window, $interval, $document, $timeout, $jsonservice) {
	//load json model
	$jsonservice.loadConfigJson("json/config.json");

	//main controller overall for all app views
	$scope.selectedMenu = "news-link";
	$scope.whileClicked = false;

	$scope.highlightMenu = function(menuId, clicked) {
		$scope.whileClicked = clicked;
		if (menuId != $scope.selectedMenu) {
			var leftPos = $("#" + menuId).position().left;
			var width = $("#" + menuId).width();
			$(".header-menu-hl").animate({
				width : width + "px",
				left : leftPos + "px"
			}, 1200, 'easeOutExpo');
			$scope.selectedMenu = menuId;
		}
		if ($scope.whileClicked) {
			setTimeout(function() {
				$scope.whileClicked = false;
			}, 2500);
		}
	};

	//scroll handler
	$interval(function() {
		if ($scope.whileClicked === false) {
			var yPos = $window.scrollY;
			var offset = 100;

			if (yPos > ($('.content').innerHeight() - $(window).height() - 120)) {
				$scope.highlightMenu("impressions-link", false);
			} else if (yPos > $(".impressions").position().top - offset) {
				$scope.highlightMenu("impressions-link", false);
			} else if (yPos > $(".music").position().top - offset) {
				$scope.highlightMenu("music-link", false);
			} else if (yPos > $(".live").position().top - offset) {
				$scope.highlightMenu("live-link", false);
			} else if (yPos < $(".live").position().top - offset) {
				$scope.highlightMenu("news-link", false);
			}
		}
	}, 1000);
	
	//lightbox impressum
	$('.impressum-popup').magnificPopup({
		type : 'inline',
		preloader : false,
	});
	
	//mobile menu
	$scope.showMobileMenu = false;
	$scope.mobileMenuSlideClass = "hide-mobile.menu";
	
	$scope.toggleMobileMenu = function(){
		if($scope.showMobileMenu){
			$scope.showMobileMenu = false;
			$scope.mobileMenuSlideClass = "hide-mobile-menu";
		}else{
			$scope.showMobileMenu = true;
			$scope.mobileMenuSlideClass = "show-mobile-menu";
		}
	};
});
