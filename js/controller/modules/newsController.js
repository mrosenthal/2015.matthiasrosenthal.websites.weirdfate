/**
 * @ngdoc overview
 * @name js.controller.view.js:NewsController
 * @module js.controller.modules
 * @description
 * News Controller
 */

application.controller('NewsController', function($scope, $rootScope, $window, $sce) {
	
	$scope.newsIndex = 0;
	$scope.showMoreNews = function(){
		$scope.newsIndex++;
		
		//rerender backgrounds
		setTimeout(function() {
			$(".background").height(($(".content").height()) / 3);
			$(".background-container").height($(".content").height());
		}, 600);
	};
	$scope.showLessNews = function(){
		$scope.newsIndex--;
		
		//rerender backgrounds
		setTimeout(function() {
			$(".background").height(($(".content").height()) / 3);
			$(".background-container").height($(".content").height());
		}, 600);
	};
});
