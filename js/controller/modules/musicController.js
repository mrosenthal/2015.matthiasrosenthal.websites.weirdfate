/**
 * @ngdoc overview
 * @name js.controller.view.js:MusicController
 * @module js.controller.modules
 * @description
 * Music Controller
 */

application.controller('MusicController', function($scope, $rootScope, $window, $timeout) {	
	$scope.$watch(function() {
		return $rootScope.jsonModel;
		}, function() {
			if($rootScope.jsonModel != undefined){
				$scope.jsonModel = $rootScope.jsonModel;
				
				//init player
				var playlist = $rootScope.jsonModel.myPlaylist;
				$scope.initMusicPlayer(playlist);
			}
		}, true);

	
	$scope.initMusicPlayer = function(playlist){
		$('.music-player').ttwMusicPlayer(playlist, {
			autoPlay : false,
			description : "",
			jPlayer : {
				swfPath : 'js/libs/jquery-jplayer/jquery-jplayer' //You need to override the default swf path any time the directory structure changes
			}
		});
	};
});