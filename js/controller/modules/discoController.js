/**
 * @ngdoc overview
 * @name js.controller.view.js:DiscoController
 * @module js.controller.modules
 * @description
 * Disco Controller
 */

application.controller('DiscoController', function($scope, $rootScope, $window, $timeout) {
	$scope.discoModel;
	$scope.setDiscographyPopup = function(discoModel){
		$scope.discoModel = discoModel;
	};
	
	$scope.showLyrics = false;
	
	$scope.toggleLyrics = function(){
		if($scope.showLyrics){
			$scope.showLyrics = false;
		}else{
			$scope.showLyrics = true;
		}
	};
});