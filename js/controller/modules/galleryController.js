/**
 * @ngdoc overview
 * @name js.controller.view.js:GalleryController
 * @module js.controller.modules
 * @description
 * Gallery Controller
 */

application.controller('GalleryController', function($scope, $rootScope, $window, $timeout) {
	$scope.galleryJson;
	
	$scope.$watch(function() {
		return $rootScope.jsonModel;
		}, function() {
			if($rootScope.jsonModel != undefined){
				$scope.galleryJson = $rootScope.jsonModel.galleryJson;
				
				//init gallery
				$scope.initGallery();
			}
		}, true);
		
	$scope.initGallery = function() {
		$timeout(function() {
		//lightbox images
		$('.slider').magnificPopup({
			delegate : 'a',
			type : 'image',
			tLoading : 'Loading image #%curr%...',
			mainClass : 'mfp-img-mobile',
			gallery : {
				enabled : true,
				navigateByImgClick : true,
				preload : [0, 1] // Will preload 0 - before current, and 1 after the current image
			},
			image : {
				tError : 'The image could not be loaded.',
				titleSrc : function(item) {
					return item.el.attr('title');
				}
			}
		});

		//lightbox discography
		$('.discography-popup').magnificPopup({
			type : 'inline',
			preloader : false,
		});
		//impressum popup
		$('.impressum-popup').magnificPopup({
			type : 'inline',
			preloader : false,
		});

		//init slider
		$('.slider').bxSlider({
			slideWidth : 85,
			minSlides : 0,
			maxSlides : 7,
			slideMargin : 12
		});
	}, 0);
	};
});
