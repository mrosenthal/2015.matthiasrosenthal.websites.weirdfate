angular.module('jsonService', []).factory('$jsonservice', ['$window', '$rootScope', '$http', 'ngProgress',
function($window, $rootScope, $http, ngProgress) {
	return {
		loadConfigJson : function(jsonUrl) {
			$http.get(jsonUrl).then(function(res) {
				$rootScope.jsonModel = res.data;
				//console.log($rootScope.jsonModel);

				setTimeout(function() {
					ngProgress.complete();

					//set bg height manually
					setTimeout(function() {
						$(".background").height(($(".content").height()) / 3);
						$(".background-container").height($(".content").height());
						//hide loading overlay
						$(".loading-overlay").fadeOut(1000);

						//init parallax backgrounds
						$.stellar({
							horizontalScrolling : false,
							verticalOffset : 40
						});
					}, 1000);
				}, 1000);
			});
		}
	};
}]);
